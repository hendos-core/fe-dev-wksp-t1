# CORE FrontEnd Dev Workspace #

Source for docker image used as dev (default) env of Codenvy workspace,
Codenvy workspace config.

[![Contribute](http://beta.codenvy.com/factory/resources/codenvy-contribute.svg)](http://beta.codenvy.com/f?url=https://gitlab.com/hendos-core/fe-dev-wksp-t1)
